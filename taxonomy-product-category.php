<?php

get_header(); ?>
<?php
/*
 * Get current taxonomy
 */
$cur_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$term_children = get_terms( array(
    'taxonomy' => 'product-category',
    'parent' => $cur_term->term_id,
    'orderby' => 'meta_value',
    'meta_key' => 'order'
) );


global $sitepress;
$current_language = $sitepress->get_current_language();
if(count($term_children) != 0):


    ?>
    <div class="header-category">
        <div class="parallax-scene" id="scene">
            <div class="header-bg-layer-1" data-depth="0.2"></div>
            <!-- /.header-bg-layer-1 -->
            <div class="header-bg-layer-2" data-depth="0.1"></div>
            <!-- /.header-bg-layer-2 -->
            <div class="header-bg-layer-3" data-depth="0.3"></div>
            <!-- /.header-bg-layer-2 -->
        </div>
        <div class="container">
            <h1><?php echo $cur_term->name; ?> GEMINI</h1>
            <p><?php the_field('cat_sub_title', $cur_term); ?></p>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-category -->
    <div class="subcategories-section">
        <div class="container">
            <div class="subcategories-block">
                <?php
                /*
                 * Get subcategories
                 */


                foreach( $term_children as $child_term ):
                    ?>
                    <!-- /.subcategories-item -->
                    <a href="<?php echo get_term_link( $child_term, 'product-category' ); ?>" class="subcategories-item">
                        <div class="subcategories-img-wrap">
                            <?php $image_src = get_field('cat_img', $child_term ); ?>
                            <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                        </div>
                        <!-- /.subcategories-img-wrap -->
                        <h3><?php echo $child_term->name; ?></h3>
                        <p>
                            <?php echo $child_term->description;  ?>
                        </p>
                    </a>
                <?php endforeach; ?>
                <!-- /.subcategories-item -->
            </div>
            <!-- /.subcategories-block -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.subcategories-section -->


    <div class="shop-section">
        <div class="container">
            <div class="shop-block">
                <?php
                /*
                 * Get all product_category
                 */





                $terms = get_terms( array(
                    'taxonomy' => 'product-category',
                    'parent' => 0,
                    'orderby' => 'meta_value',
                    'include'  => array('37', '47', '27', '6', '56', '7'),
                    'meta_key' => 'order'
                ) );
                


                ?>
                <?php foreach($terms as $term): ?>
                    <a href="<?php echo get_term_link($term->term_id); ?>" class="shop-item tea-item">
                        <?php $image_src = get_field('cat_img', $term ); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                        <strong>
                            <?php echo $term->name; ?>
                        </strong>
                        <p>
                            <?php  echo $term->description; ?>
                        </p>
                    </a>
                <?php endforeach; ?>
                <!-- /.shop-item -->

                <!-- /.shop-item -->
                <a href="<?php the_field('href_online_shop','option'); ?>" class="shop-link">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png"
                         srcset="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png 1x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@2x.png 2x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@3x.png 3x" alt="image">
                    <strong>
                        <?php the_field('online_shop', 'option'); ?>
                    </strong>
                </a>
            </div>
        </div>
    </div>

    <?php  get_template_part( 'template-parts/advantages-section'); ?>


    <div class="news-section">
        <div class="container">
            <h2>
                <?php echo __('News','gemini'); ?>
            </h2>

            <div class="news-block">
                <?php
                $args = array(
                    'order' => 'DESC',
                    'posts_per_page' => 4
                );
                $the_query = new WP_Query($args);
                while ( $the_query->have_posts()) :
                    $the_query->the_post(); ?>
                    <div class="news-item">
                        <div class="news-img-wrap">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <!-- /.news-img-wrap -->
                        <div class="news-text-wrap">
                            <a href="<?php the_permalink(); ?>"></a>
                            <h3><?php the_title(); ?></h3>
                            <span class="public-date">
                        <?php echo get_the_date(); ?>
                    </span>
                            <!-- /.public-date -->
                            <p>
                                <?php
                                $content = get_the_content();
                                echo wp_trim_words( $content , 40, '...' );
                                ?>
                                <?php global $sitepress;
                                $current_language = $sitepress->get_current_language();?>
                                <a href="<?php the_permalink(); ?>"><?php if($current_language == 'uk'):
                                        echo 'ЩЕ';
                                    elseif($current_language == 'en'):
                                        echo 'MORE';
                                    else: echo 'ЕЩЁ';
                                    endif; ?>
                                </a>
                            </p>
                        </div>
                        <!-- /.news-text-wrap -->
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>
            </div>
            <?php
            if (get_field('add_cat_desc', $cur_term)) { ?>
                <div class="wrap-category-description">
                    <div class="category-description">
                        <?php the_field('add_cat_desc', $cur_term); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php  get_template_part( 'template-parts/callback-section'); ?>


<?php else: ?>

    <div class="header-subcategory">
        <div class="parallax-scene" id="scene">
            <div class="header-bg-layer-1" data-depth="0.2"></div>
            <!-- /.header-bg-layer-1 -->
            <div class="header-bg-layer-2" data-depth="0.1"></div>
            <!-- /.header-bg-layer-2 -->
            <div class="header-bg-layer-3" data-depth="0.3"></div>
            <!-- /.header-bg-layer-2 -->
        </div>
        <div class="container">
            <h1><?php echo $cur_term->name; ?></h1>
            <p><?php the_field('cat_sub_title', $cur_term); ?></p>
            <a href="#" onclick="window.location.href = history.back();" class="left-arrow">
                <?php echo __('Back','gemini'); ?>
            </a>
            <a href="<?php the_field('href_online_shop', 'option') ?>" class="btn">
                <?php echo __('In shop','gemini'); ?>
            </a>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.header-subcategory -->
    <div class="subcategories-product-section">
        <div class="container">
            <div class="products-block">

                <?php
                /*
                 * Select all products from current term
                 */
                $args = array('post_type' => 'product',
                    'order'   => 'DESC',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product-category',
                            'field' => 'name',
                            'terms'     => $cur_term->name,
                        ),
                    ),
                );
                $the_query = new WP_Query($args);
                while ( $the_query->have_posts()) :
                    $the_query->the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="product-item">
                        <div class="product-item-img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <!-- /.product-item-img -->
                        <p><?php the_title(); ?></p>
                    </a>
                    <!-- /.product-item -->
                <?php endwhile; ?>


            </div>
            <!-- /.subcategories-product-block -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.subcategories-product-section -->
    <div class="subcategories-section">
        <div class="container">
            <div class="subcategories-block subcategory-page">
                <?php
                /*
                 * Get sub categories
                 */

                $term_children = get_term_children( $cur_term->parent, 'product-category' );

                foreach( $term_children as $child ):
                    $child_term = get_term_by( 'id', $child,'product-category');
                    if(!empty($child_term) && $child_term->name != $cur_term->name ):
                        ?>
                        <!-- /.subcategories-item -->
                        <a href="<?php echo get_term_link( $child_term, 'product-category' ); ?>" class="subcategories-item">
                            <div class="subcategories-img-wrap">
                                <?php $image_src = get_field('cat_img', $child_term ); ?>
                                <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                            </div>
                            <!-- /.subcategories-img-wrap -->
                            <h3><?php echo $child_term->name; ?></h3>
                            <p>
                                <?php echo $child_term->description;  ?>
                            </p>
                        </a>
                    <?php endif;; ?>
                <?php endforeach; ?>

            </div>
            <!-- /.subcategories-block -->
            <?php
            if (get_field('add_cat_desc', $cur_term)) { ?>
                <div class="wrap-category-description news-section">
                    <div class="category-description">
                        <?php the_field('add_cat_desc', $cur_term); ?>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.subcategories-section -->

    <!-- /.callback-section -->

    <?php  get_template_part( 'template-parts/callback-section'); ?>

    <!-- /.callback-section -->


<?php endif; ?>


<?php get_footer(); ?>
