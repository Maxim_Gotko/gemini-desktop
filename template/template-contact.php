<?php
/**
 * Template Name: contact
 *
 */

get_header(); $page_id = get_the_ID();  ?>

<div class="header-contacts">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/contacts-map.svg" alt="image">
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->

<!-- /.callback-section -->

<?php get_template_part('template-parts/callback-section'); ?>

<!-- /.callback-section -->
<div class="container">
<!--    <div class="contact-block">-->
<!--        <div class="contact-img">-->
<!--            <img src="--><?php //the_field('contact_img', $page_id); ?><!--" alt="image">-->
<!--        </div>-->
<!--        <!-- /.contact-img -->
<!--        --><?php //while (have_rows('contact_info_1', $page_id)) : the_row(); ?>
<!---->
<!--            <div class="contact-info">-->
<!--                <ul>-->
<!--                    <li class="icon-map">-->
<!--                        <strong>--><?php //the_sub_field('title_adress'); ?><!--</strong>-->
<!---->
<!--                        <p>--><?php //the_sub_field('adress'); ?><!--</p>-->
<!--                    </li>-->
<!--                    <li class="icon-phone">-->
<!--                        <strong>--><?php //the_sub_field('title_phone'); ?><!--</strong>-->
<!--                        --><?php //the_sub_field('phones'); ?>
<!--                    </li>-->
<!--                    <li class="icon-email">-->
<!--                        <strong>--><?php //the_sub_field('title_mail'); ?><!--</strong>-->
<!---->
<!--                        <p>--><?php //the_sub_field('mail'); ?><!--</p>-->
<!--                    </li>-->
<!--                    <li class="icon-time">-->
<!--                        <strong>--><?php //the_sub_field('title_work_time'); ?><!--</strong>-->
<!---->
<!--                        <p>--><?php //the_sub_field('work_time'); ?><!--</p>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </div>-->
<!---->
<!--        --><?php //endwhile; ?>
<!--        <!-- /.contact-info -->
<!--    </div>-->
    <!-- /.contacts-block -->
    <div class="contact-block contact-gallery">
        <div class="contact-slider">
            <div class="swiper-wrapper">
                <?php while (have_rows('slider_1', $page_id)) : the_row(); ?>

                    <div class="swiper-slide contact-img">
                        <img src="<?php the_sub_field('img'); ?>" alt="image">
                    </div>

                <?php endwhile; ?>
            </div>
            <!-- /.swiper-wrapper -->
            <div class="gallery-button-next"><span></span></div>
            <div class="gallery-button-prev"><span></span></div>
        </div>
        <!-- /.contact-img -->
        <?php while (have_rows('contact_info_2', $page_id)) : the_row(); ?>

            <div class="contact-info">
                <ul>
                    <li class="icon-map">
                        <strong><?php the_sub_field('title_adress'); ?></strong>

                        <p><?php the_sub_field('adress'); ?></p>
                    </li>
                    <li class="icon-phone">
                        <strong><?php the_sub_field('title_phone'); ?></strong>
                        <?php the_sub_field('phones'); ?>
                    </li>
                    <li class="icon-email">
                        <strong><?php the_sub_field('title_mail'); ?></strong>

                        <p><?php the_sub_field('mail'); ?></p>
                    </li>
                    <li class="icon-time">
                        <strong><?php the_sub_field('title_work_time'); ?></strong>

                        <p><?php the_sub_field('work_time'); ?></p>
                    </li>
                </ul>
            </div>

        <?php endwhile; ?>
        <!-- /.contact-info -->
    </div>
    <!-- /.contacts-block -->
    <div class="contact-block contact-gallery">
        <div class="contact-slider">
            <div class="swiper-wrapper">
                <?php while (have_rows('slider_2', $page_id)) : the_row(); ?>

                    <div class="swiper-slide contact-img">
                        <img src="<?php the_sub_field('img'); ?>" alt="image">
                    </div>

                <?php endwhile; ?>
            </div>
            <!-- /.swiper-wrapper -->
            <div class="gallery-button-next"><span></span></div>
            <div class="gallery-button-prev"><span></span></div>
        </div>
        <!-- /.contact-img -->
        <?php while (have_rows('contact_info_3', $page_id)) : the_row(); ?>

            <div class="contact-info">
                <ul>
                    <li class="icon-map">
                        <strong><?php the_sub_field('title_adress'); ?></strong>

                        <p><?php the_sub_field('adress'); ?></p>
                    </li>
                    <li class="icon-phone">
                        <strong><?php the_sub_field('title_phone'); ?></strong>
                        <?php the_sub_field('phones'); ?>
                    </li>
                    <li class="icon-email">
                        <strong><?php the_sub_field('title_mail'); ?></strong>

                        <p><?php the_sub_field('mail'); ?></p>
                    </li>
                    <li class="icon-time">
                        <strong><?php the_sub_field('title_work_time'); ?></strong>

                        <p><?php the_sub_field('work_time'); ?></p>
                    </li>
                </ul>
            </div>

        <?php endwhile; ?>
        <!-- /.contact-info -->
    </div>
</div>
<!-- /.container -->
<?php get_footer(); ?>
