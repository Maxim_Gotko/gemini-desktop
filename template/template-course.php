<?php
/**
 * Template Name: course
 *
 */

get_header(); $page_id = get_the_ID(); ?>

<div class="header-tasting">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <a href="javascript:history.go(-1);" class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>

<!-- /.course-section -->
<div class="course-section">
    <div class="container">
        <div class="tasting-info">
            <?php the_content(); ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.course-section -->



<!-- /.callback-section -->

<?php  get_template_part( 'template-parts/callback-section'); ?>

<!-- /.callback-section -->

<?php get_footer(); ?>
