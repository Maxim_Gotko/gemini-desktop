<?php
/**
 * The template part for displaying advantages-section content
 */
?>
<div class="statistics-section">
    <div class="container">
        <?php while ( have_rows('advantages','option') ) : the_row(); ?>
        <h2><?php the_sub_field('title_adv_block'); ?></h2>
        <div class="statistics-block">
            <div class="statistics-item ">
                <div class="statistics-image-wrap statistics-year">
                    <?php $image_src = get_sub_field('adv_img_1'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                </div>
                <!-- /.advantage-image-wrap -->
                <strong>
                    <?php the_sub_field('adv_number_1'); ?>
                    <small><?php the_sub_field('adv_number_desc_1'); ?></small>
                    <span class="shadow_effect">
                        <?php the_sub_field('adv_number_1'); ?><small><?php the_sub_field('adv_number_desc_1'); ?></small>
                    </span>
                    <!-- /.shadow_effect -->
                </strong>
                <p>
                    <?php the_sub_field('adv_desc_1'); ?>
                </p>
            </div>
            <!-- /.advantage-item -->
            <div class="statistics-item">
                <div class="statistics-image-wrap statistics-weight">
                    <?php $image_src = get_sub_field('adv_img_2'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                </div>
                <!-- /.advantage-image-wrap -->
                <strong>
                    <?php the_sub_field('adv_number_2'); ?>
                    <small><?php the_sub_field('adv_number_desc_2'); ?></small>
                    <span class="shadow_effect">
                         <?php the_sub_field('adv_number_2'); ?><small><?php the_sub_field('adv_number_desc_2'); ?></small>
                    </span>
                    <!-- /.shadow_effect -->
                </strong>
                <p>
                    <?php the_sub_field('adv_desc_2'); ?>
                </p>
            </div>
            <!-- /.advantage-item -->
            <div class="statistics-item">
                <div class="statistics-image-wrap statistics-machine">
                    <?php $image_src = get_sub_field('adv_img_3'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                </div>
                <!-- /.advantage-image-wrap -->
                <strong>
                    <?php the_sub_field('adv_number_3'); ?>
                    <span class="shadow_effect">
                      <?php the_sub_field('adv_number_3'); ?>
                    </span>
                    <!-- /.shadow_effect -->
                </strong>
                <p>
                    <?php the_sub_field('adv_desc_3'); ?>
                </p>
            </div>
            <!-- /.advantage-item -->
            <div class="statistics-item">
                <div class="statistics-image-wrap statistics-service">
                    <?php $image_src = get_sub_field('adv_img_4'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                </div>
                <!-- /.advantage-image-wrap -->
                <strong>
                    <?php the_sub_field('adv_number_4'); ?>
                    <span class="shadow_effect">
                  <?php the_sub_field('adv_number_4'); ?>
                    </span>
                    <!-- /.shadow_effect -->
                </strong>
                <p>
                    <?php the_sub_field('adv_desc_4'); ?>
                </p>
            </div>
            <!-- /.advantage-item -->
        </div>
        <!-- /.advantage-block -->
    </div>
    <!-- /.container -->
    <?php endwhile; ?>
</div>