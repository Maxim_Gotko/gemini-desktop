<?php
/**
 * The template part for displaying posts
 */
?>
<div class="news-item">
    <a href="<?php the_permalink(); ?>" class="news-img-wrap">
        <?php the_post_thumbnail(); ?>
    </a>
    <!-- /.news-img-wrap -->
    <div class="news-text-wrap">
        <a href="<?php the_permalink(); ?>"></a>
        <h3><?php the_title(); ?></h3>
                        <span class="public-date">
                            <?php echo get_the_date(); ?>
                        </span>
        <!-- /.public-date -->
        <p>
            <?php
            $content = get_the_content();
            echo wp_trim_words( $content , 40, '...' );
            ?>
            <?php global $sitepress;
            $current_language = $sitepress->get_current_language();?>
            <a href="<?php the_permalink(); ?>"><?php echo __('MORE','gemini'); ?>
            </a>
        </p>
    </div>
    <!-- /.news-text-wrap -->
</div>