<?php
/**
 * The template part for displaying sidebar-post-content
 */
?>  <a href="<?php the_permalink(); ?>">
    <h3><?php the_title(); ?></h3>
                <span class="post-date">
                 <?php echo get_the_date(); ?>
                </span>
    <!-- /.post-date -->
</a>