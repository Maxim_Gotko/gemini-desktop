<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="section-404">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1><?php echo __('Coffee ended :(','gemini'); ?></h1>
        <div class="main-404">
            <span>4</span>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/404.png" alt="0">
            <span>4</span>
        </div>
        <!-- /.main404 -->
        <div class="info-404">
            <strong><?php echo __('What to do?','gemini'); ?></strong>
            <ul>
                <li><?php echo __('1. Return to','gemini'); ?><a href=" <?php echo get_home_url(); ?>" class="to-main">
                        <?php echo __('main page','gemini'); ?></a></li>
                <li><?php echo __('2. Write us everything you think ','gemini'); ?><a href="mailto :info@gemini.ua">
                        <?php echo __('info@gemini.ua','gemini'); ?>
                    </a></li>
                <li><?php echo __('3. Continue looking on the screen and after 30sec you will be automatically redirected to the main page.','gemini'); ?></li>
            </ul>
        </div>
        <!-- /.info-404 -->
    </div>
    <!-- /.container -->
</div>
<script type="text/javascript">
    setTimeout(function() { window.location.href = '//gemini.ua'; }, 30000);
</script>


<?php get_footer(); ?>
