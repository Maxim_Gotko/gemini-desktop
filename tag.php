<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="header-news">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1><?php echo __('News','gemini'); ?></h1>
        <a href="javascript:history.go(-1); " class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="news-post-section">
    <div class="container no-padding-right">
        <div class="news-post-block news-all-post-block">
            <div class="news-block news-all-posts">

                <?php

                // Start the loop.
                if(have_posts()) :
                    while (have_posts()) : the_post();

                        get_template_part('template-parts/post-preview');

                    endwhile;
                else :
                endif;
                ?>

            </div>
            <!-- /.news-post -->
            <div class="news-sidebar">
                <form class="search">
                    <div>
                        <input type="text"  class="search-input">
                        <input type="submit" id="searchsubmit" value="Search">
                    </div>
                    <label for="">
                        <?php echo __('Select category','gemini'); ?>
                    </label>
                    <select name="" class="search-select-input" id="">
                        <?php
                        /*
                         * Select all post category
                         */
                        $terms = get_terms( array(
                            'taxonomy' => 'category',
                            'order' => 'DESC'
                        ) );?>
                        <option value="none"></option>
                        <?php foreach($terms as $term): ?>
                            <option value="<?php echo $term->term_id;  ?>"><?php echo $term->name;  ?></option>
                        <?php endforeach; ?>
                    </select>
                    <!-- /# -->
                </form>
                <div class="swiper-scroll-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <?php
                            /*
                             * Select * posts
                             */
                            $args = array(
                                'order' => 'DESC'
                            );
                            $the_query = new WP_Query($args);
                            while ( $the_query->have_posts()) :
                                $the_query->the_post();
                                get_template_part('template-parts/sidebar-post-content');
                            endwhile; ?>
                        </div>
                        <!-- /.swiper-slide -->
                    </div>
                    <!-- /.swiper-wrapper -->
                    <div class="swiper-scrollbar"></div>
                    <!-- /.swiper-scrollbar -->
                </div>
                <!-- /.swiper-scroll-container -->
                <div class="facebook-block">
                    <h3><?php echo __('Find Us on Facebook','gemini'); ?></h3>

                    <div id="fb-root"></div>
                    <div class="fb-page" data-href="https://www.facebook.com/espressogemini/"
                         data-small-header="false" data-adapt-container-width="true"
                         data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/espressogemini/"
                                    class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/espressogemini/">
                                Gemini Espresso</a>
                        </blockquote>
                    </div>
                </div>
                <!-- /.facebook-block -->
                <div class="social-link">
                    <h3>
                        <?php echo __('Join Us','gemini'); ?>
                    </h3>
                    <a href="<?php the_field('facebook', 'option'); ?>" class="icon-fb"></a>
                    <a href="<?php the_field('instagram', 'option'); ?>" class="icon-instagram"></a>
                    <a href="<?php the_field('twitter', 'option'); ?>" class="icon-twitter"></a>
                    <a href="<?php the_field('youtube', 'option'); ?>" class="icon-youtube-symbol"></a>
                    <a href="<?php the_field('pinterest', 'option'); ?>" class="icon-pinterest"></a>
                    <a href="<?php the_field('google', 'option'); ?>" class="icon-google-plus"></a>
                    <!-- /.email -->
                </div>
                <div id="secondary" class="sidebar-container" role="complementary">
                    <div class="widget-area">
                        <?php dynamic_sidebar( 'news-sidebar-id' ); ?>
                    </div><!-- .widget-area -->
                </div><!-- #secondary -->
                <!-- /.social-links -->
            </div>
            <!-- /.news-sidebar -->
        </div>
        <!-- /.news-post-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.news-post-section -->

<!-- /.callback-section -->

<?php  get_template_part('template-parts/callback-section'); ?>

<!-- /.callback-section -->


<!---->



<?php get_footer(); ?>
