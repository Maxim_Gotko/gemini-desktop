
$(document).ready(function () {
    if($('.map').length == 1){
        initMap();
    }

    if($('#scene').length == 1){
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);
    }

    var swiper = new Swiper('.swiper-scroll-container', {
        scrollbar: '.swiper-scrollbar',
        direction: 'vertical',
        slidesPerView: 'auto',
        mousewheelControl: true,
        freeMode: true,
        scrollbarHide:false,
        scrollbarDraggable:true
    });

    var galleryTop = new Swiper('.gallery-top', {
        nextButton: '.gallery-button-next',
        prevButton: '.gallery-button-prev',
        spaceBetween: 0,
        slidesPerView: 'auto',
        observer:true
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 5,
        centeredSlides: true,
        slidesPerView: 'auto',
        touchRatio: 0.2,
        slideToClickedSlide: true
    });
    galleryTop.params.control = galleryThumbs;
    galleryThumbs.params.control = galleryTop;

    var menuItem = $('.menu > .menu-item > a');

    menuItem.on('click', function (e) {
        if($(this).next().hasClass('sub-menu')){
            e.preventDefault();
            $('.menu-item a').removeClass('active');
            $(this).addClass('active');
            $(this).next('.sub-menu').addClass('open');
            $('body').addClass('submenu-open');
        }

    });

    $('#switcher_mobile').on('click', function() {
        var name = 'mobile-smart-switcher';
        document.cookie = name +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = name +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=m.gemini.ua";
        document.cookie = name +"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;domain=gemini.ua";
        location.reload();

    });

    $(document).on('click', function (event) {
        if ($(event.target).closest('.menu-item').length == 0) {
            $('.sub-menu').removeClass('open');
            $('body').removeClass('submenu-open');
        }
    });
    var gallery = new Swiper('.contact-slider', {
        nextButton: '.gallery-button-next',
        prevButton: '.gallery-button-prev',
        spaceBetween: 0,
        slidesPerView: 'auto',
        observer:true
    });

    $('.header-image').on('click',function () {
        $('.popup-video').fadeIn();
        $('body').addClass('video-open');
    });

    function closeVideo() {
        var content = $('.popup-video')[0];
        if (content) {
            $('.popup-video').fadeOut('fast');
            $('body').removeClass('video-open');
            $('.popup-video iframe').attr( 'src', function ( i, val ) { return val; });
        }
    }

    $('.video-wrap .close').on('click', function () {
        closeVideo();
    });

    $('body').on('click', '.popup-video', function () {
        closeVideo();
    })
});





var centerMap = {lat: -25.363, lng: 131.044};
var mapLng = '131.044';
var mapLat = '-25.363';
var selectAddress = $('.map-address :selected');
mapLng = selectAddress.data('map-lng');
mapLat = selectAddress.data('map-lat');

$('.map-address').on('change', function () {
    var selectAddress = $('.map-address :selected');
    mapLng = selectAddress.data('map-lng');
    mapLat = selectAddress.data('map-lat');
    changeMapPosition();
});
var marker;
var map;
function initMap() {
    var styleMap = [
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e9e9e9"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "landscape",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 17
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 29
                },
                {
                    "weight": 0.2
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 18
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dedede"
                },
                {
                    "lightness": 21
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#ffffff"
                },
                {
                    "lightness": 16
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "saturation": 36
                },
                {
                    "color": "#333333"
                },
                {
                    "lightness": 40
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f2f2f2"
                },
                {
                    "lightness": 19
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 20
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#fefefe"
                },
                {
                    "lightness": 17
                },
                {
                    "weight": 1.2
                }
            ]
        }
    ];
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat:  50.414031, lng: 30.521520 - 0.028 },
        styles: styleMap
    });
    var imageUrl = img_src;
    var image = {
        url:imageUrl,
        size: new google.maps.Size(100, 100),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(25, 50)
    };
    marker = new google.maps.Marker({
        position: {lat: 50.414031, lng: 30.521520},
        map: map,
        icon: image
    });

}
function changeMapPosition() {
    marker.setPosition({lat: mapLat, lng: mapLng});
    map.setCenter({lat: mapLat, lng: mapLng - 0.028})
}

$('.scroll-down').on('click', function () {
    var height = $(window).height();
    $('html, body').animate({scrollTop : height + 10},800);
    return false;
});

$(window).on('scroll', function () {
    var height = $(window).height();
    if ($(document).scrollTop() > 87){
        $('.main-header').addClass('header-fixed');
    }else{
        $('.main-header').removeClass('header-fixed');
    }
    if ($(document).scrollTop() > 150){
        $('.main-header').addClass('header-transition');
    }else{
        $('.main-header').removeClass('header-transition');
    }
    if($(document).scrollTop() > height){
        $('.main-header').addClass('header-show');
    }else {
        $('.main-header').removeClass('header-show');
    }
});

// Get the modal

