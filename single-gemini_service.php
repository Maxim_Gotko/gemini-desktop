<?php

get_header(); $page_id = get_the_ID(); ?>

<div class="header-tasting">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <a href="javascript:history.go(-1);" class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->
<div class="container">
    <?php while (have_posts()) : the_post(); ?>
        <div class="tasting-info">
            <?php $gallery = get_field('service-gallery', $page_id ); ?>
            <div class="gallery-block">
                <div class="swiper-container gallery-top">
                    <div class="swiper-wrapper">
                        <?php while ( have_rows('service-gallery', $page_id ) ) : the_row(); ?>
                            <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                        <?php endwhile; ?>

                    </div>
                    <!-- Add Arrows -->
                    <div class="gallery-button-next"><span></span></div>
                    <div class="gallery-button-prev"><span></span></div>
                </div>
                <div class="swiper-container gallery-thumbs">
                    <div class="swiper-wrapper">
                        <?php while ( have_rows('service-gallery', $page_id ) ) : the_row(); ?>
                            <div class="swiper-slide" style="background-image:url(<?php the_sub_field('img'); ?>)"></div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php the_content(); ?>
        </div>
    <?php endwhile; ?>
    <!-- /.html -->
</div>
<!-- /.container -->

<div class="services-section">
    <div class="container">
        <div class="statistics-block">

            <?php
            $args = array(
                'post_type' => 'gemini_service',
                'posts_per_page' => 4,
                'orderby' => 'rand',
                'post__not_in' => array(get_the_ID())
            );

            $query = new WP_Query( $args );

            if ( $query->have_posts() ) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>
                    <a href="<?php echo get_permalink(); ?>" class="statistics-item ">
                        <div class="statistics-image-wrap statistics-year">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" srcset="<?php echo get_the_post_thumbnail_url(); ?> , <?php echo get_srcset_by_img_src(get_the_post_thumbnail_url()); ?>" alt="image">                </div>
                        <!-- /.advantage-image-wrap -->
                        <strong>
                            <?php echo get_the_title(); ?>
                            <span class="shadow_effect">
                        <?php echo get_the_title(); ?>
                    </span>
                            <!-- /.shadow_effect -->
                        </strong>
                        <p><?php echo the_excerpt(); ?></p>

                    </a>
                    <?php
                }
            } else {

            }
            wp_reset_postdata();

            ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<!-- /.services-section -->



<!-- /.callback-section -->

<?php  get_template_part( 'template-parts/callback-section'); ?>

<!-- /.callback-section -->

<!-- /.callback-section -->
<?php get_footer(); ?>
